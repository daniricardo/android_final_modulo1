package dani.com.py.examenm1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * Created by Dell on 04/12/2017.
 */

public class ResetPassActivity extends AppCompatActivity {
    EditText emailResetPass;
    Button resetPass;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pass);

        emailResetPass = (EditText) findViewById(R.id.edit_email_reset_pass);
        resetPass = (Button) findViewById(R.id.btn_reset_pass);


        resetPass.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(emailResetPass.getText().toString().equals("")){
                    emailResetPass.setError("Email debe ser llenado");
                }else{
                    Toast toast = Toast.makeText(getApplicationContext(), "Su contraseña ha sido cambiada, verifique su email", Toast.LENGTH_LONG);
                    toast.show();

                }


                //se limpian campos luego de confirmar envio de formulario
                emailResetPass.setText("");

            }//fin onClick
        }); //fin setOnClickListener

        //toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar_reset_pass);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
    }// fin onCreate

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }
}
