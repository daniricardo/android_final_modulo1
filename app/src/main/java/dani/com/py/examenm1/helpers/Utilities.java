package dani.com.py.examenm1.helpers;

import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

/**
 * Created by Dell on 28/11/2017.
 */

public class Utilities {

    public static void checkLocale(Context ctx, String languageToLoad){ // ONLY FOR API < 17

        Locale locale = new Locale (languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        ctx.getResources().updateConfiguration(config, ctx.getResources().getDisplayMetrics());
    }
}
