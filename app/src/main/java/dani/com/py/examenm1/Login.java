package dani.com.py.examenm1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import dani.com.py.examenm1.R;
import dani.com.py.examenm1.entities.Peliculas;
import dani.com.py.examenm1.helpers.Utilities;

/**
 * Created by Dell on 27/11/2017.
 */

public class Login  extends AppCompatActivity {
    EditText edit_mail;
    EditText edit_pass;
    Button btn_sign_in;
    SharedPreferences preferences;
    View activity_sign_up;
    Button btn_sign_up;
    TextView reset_pass;
    Button btn_fb;
    Button btn_twi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edit_mail = (EditText) findViewById(R.id.edit_user);
        edit_pass = (EditText) findViewById(R.id.edit_pass);
        btn_sign_in = (Button) findViewById(R.id.btn_log_in);
        activity_sign_up = findViewById(R.id.activity_login);
        btn_sign_up = (Button) findViewById(R.id.btn_sign_up);
        reset_pass = (TextView) findViewById(R.id.txt_reset_pass);
        btn_fb = (Button) findViewById(R.id.btn_login_fb);
        btn_twi = (Button) findViewById(R.id.btn_login_tw);

        preferences = getSharedPreferences("user_data", getApplicationContext().MODE_PRIVATE);

        if(preferences != null){
            String lan = preferences.getString("user_language", "");
            Resources res = getBaseContext().getResources();
            Log.d("LANG", lan);
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
                    Log.d("IDIOMA_CHANGE", "IDIOMA_CHANGE-->"+lan);
                    conf.setLocale(new Locale(lan));
                    res.updateConfiguration(conf, dm);
                }else{
                    Utilities.checkLocale(getApplicationContext(), lan);
                }
        }

        if(preferences != null){
            String usuario = preferences.getString("user", "");
            String pass = preferences.getString("pass", "");
            edit_mail.setText(usuario);
            //edit_pass.setText(pass);

            if(!usuario.equals("") && !pass.equals("")){
                Intent intent = new Intent(Login.this, Peliculas.class);
                startActivity(intent);
                finish();
            }
        }

        //boton de logueo
        btn_sign_in.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(edit_mail.getText().toString().equals("prueba") && edit_pass.getText().toString().equals("123")){
                    SharedPreferences.Editor toEdit;
                    toEdit = preferences.edit();
                    toEdit.putString("usuario", edit_mail.getText().toString());
                   // toEdit.putString("pass", edit_pass.getText().toString());
                    toEdit.commit();

                    Intent intent = new Intent(Login.this, MostrarPeliActivity.class);
                    startActivity(intent);
                    finish();

                }else{
                    Toast toast = Toast.makeText(getApplicationContext(), "Credenciales incorrectas", Toast.LENGTH_LONG);
                    toast.show();
                }

            }
        }); //fin setOnClickListener


        //boton de alta
        btn_sign_up.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (Login.this, SignUpActivity.class);
                startActivity(intent);
                //finish();
            }
        });


        //resetear Pass
        reset_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (Login.this, ResetPassActivity.class);
                startActivity(intent);
                //finish();
            }
        });

        //boton facebook
        btn_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(getApplicationContext(), "Usted se logueará a través de Facebook", Toast.LENGTH_LONG);
                toast.show();
                //finish();
            }
        });

        //boton Twitter
        btn_twi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(getApplicationContext(), "Usted se logueará a través de Twitter", Toast.LENGTH_LONG);
                toast.show();
                //finish();
            }
        });


    }//fin onCreate
}
