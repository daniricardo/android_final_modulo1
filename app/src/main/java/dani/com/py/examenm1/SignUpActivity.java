package dani.com.py.examenm1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Dell on 04/12/2017.
 */

public class SignUpActivity extends AppCompatActivity {
    EditText email;
    EditText pass;
    EditText confirmarPass;
    EditText nombre;
    Button crearCuenta;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        email = (EditText) findViewById(R.id.edit_email_sign_up);
        pass = (EditText) findViewById(R.id.edit_pass_sign_up);
        confirmarPass = (EditText) findViewById(R.id.edit_repass_sign_up);
        nombre = (EditText) findViewById(R.id.edit_name_sign_up);
        crearCuenta = (Button) findViewById(R.id.btn_login_tw);

        crearCuenta.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //se valida que los campos no esten vacios
                if(email.getText().toString().equals("")){
                    email.setError("Email debe ser llenado");
                }
                if(pass.getText().toString().equals("")){
                    pass.setError("Password debe ser llenado");
                }
                if(confirmarPass.getText().toString().equals("")){
                    confirmarPass.setError("Password debe ser confirmado");
                }
                if(nombre.getText().toString().equals("")){
                    nombre.setError("Nombre debe ser llenado");
                }
                Toast toast = Toast.makeText(getApplicationContext(), "Su cuenta ha sido creada, verificar su email", Toast.LENGTH_LONG);
                toast.show();

                //se limpian campos luego de confirmar envio de formulario
                email.setText("");
                pass.setText("");
                confirmarPass.setText("");
                nombre.setText("");
            } // fin onClick

        });// fin onClickListener


        //toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar_sign_up);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

    }//fin onCreate

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }
}
