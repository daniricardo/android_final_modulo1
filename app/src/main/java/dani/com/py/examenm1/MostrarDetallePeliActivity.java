package dani.com.py.examenm1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import dani.com.py.examenm1.R;

/**
 * Created by Dell on 01/12/2017.
 */

public class MostrarDetallePeliActivity extends AppCompatActivity {

    ImageView imgPortadaPeliculaDet;
    String txtTituloPeliculaDet;
    TextView txtDescripcionDet;
    TextView txtTitEstrenoDet;
    TextView txtEstrenoDet;
    TextView txtFechaEstreno;
    TextView txt_title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pelicula);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            txtTituloPeliculaDet = bundle.getString("movie_name");
        }
        imgPortadaPeliculaDet = (ImageView) findViewById(R.id.img_portada_detalle);
        txtDescripcionDet = (TextView) findViewById(R.id.descripcion_detalle);
        txtTitEstrenoDet = (TextView) findViewById(R.id.text_titulo_estreno);
        txtEstrenoDet = (TextView) findViewById(R.id.text_estreno);
        txtFechaEstreno = (TextView) findViewById(R.id.text_fecha_estreno);
        txt_title = (TextView) findViewById(R.id.text_titulo_detalle);
        //txtTituloPeliculaDet.setText(txtTituloPeliculaDet);

        Bitmap portada;

        switch (txtTituloPeliculaDet){
            case "Dawn of the Planet of the Apes":
                txtDescripcionDet.setText("En el año 2011, el virus ALZ-113 causa el colapso de la civilización humana tras una ley marcial, el malestar social y el colapso económico de todos los países del mundo. Diez años más tarde, en el 2021, el 90 % de la población mundial ha muerto y hay pequeños reductos de supervivientes en las montañas. Un simio llamado César dirige y gobierna una nueva generación de simios en una comunidad ubicada en el bosque Muir.");
                portada = BitmapFactory.decodeResource(getResources(), R.drawable.simios);
                txt_title.setText("Dawn of the Planet of the Apes");
                imgPortadaPeliculaDet.setImageBitmap(portada);
                txtTitEstrenoDet.setText("Details");
                txtEstrenoDet.setText("Released");
                txtFechaEstreno.setText("May 1, 2015");

                break;

            case "District 9":
                txtDescripcionDet.setText("La película comienza como un documental. De esta forma el espectador conoce el trasfondo de la película: una gigantesca nave alienígena llegó a la Tierra en 1982 y permaneció suspendida sobre la ciudad de Johannesburgo, en Sudáfrica. Pasados varios meses sin que se observaran señales de vida y tras varias deliberaciones los humanos decidieron abrirse paso a través del blindaje de la nave para acceder al interior. Lo que allí se encontraron fue cerca de un millón de alienígenas desorientados y desnutridos. Al principio los extraterrestres recibieron ayuda humanitaria, pero con el tiempo se vieron obligados a vagar por la ciudad revolviendo en la basura y buscando comida.");
                portada = BitmapFactory.decodeResource(getResources(), R.drawable.distrito9);
                imgPortadaPeliculaDet.setImageBitmap(portada);
                txt_title.setText("District 9");
                txtTitEstrenoDet.setText("Details");
                txtEstrenoDet.setText("Released");
                txtFechaEstreno.setText("May 1, 2015");

                break;

            case "Transformers: Age of Extinction":
                txtDescripcionDet.setText("Transformers es una película de acción y ciencia ficción estadounidense de 2007 basada en la línea de juguetes de Transformers. La película, que combina animación por computadora con acción en vivo, fue dirigida por Michael Bay, con Steven Spielberg como productor ejecutivo. Es la primera entrega de la serie fílmica de Transformers. Es protagonizada por Shia LaBeouf como Sam Witwicky, un adolescente que queda atrapado en una guerra entre los heroicos Autobots y los malvados Decepticons, dos facciones de robots extraterrestres que pueden ocultarse transformándose en maquinaria cotidiana, sobre todo vehículos. Los Autobots pretenden usar la Chispa Suprema, el objeto que creó su raza robótica, en un intento de reconstruir Cybertron y finalizar la guerra, mientras los Decepticons desean el control de la Chispa Suprema con la intención de usarla para construir un ejército dándole vida a las máquinas en la Tierra.");
                portada = BitmapFactory.decodeResource(getResources(), R.drawable.optimus);
                imgPortadaPeliculaDet.setImageBitmap(portada);
                txt_title.setText("Transformers: Age of Extinction");
                txtTitEstrenoDet.setText("Details");
                txtEstrenoDet.setText("Released");
                txtFechaEstreno.setText("May 1, 2015");

                break;

            case "X-men: Days of Future Past":
                txtDescripcionDet.setText("X-Men, también conocidos como Patrulla-X en España1\u200B y Los Hombres X en Hispanoamérica, son un grupo de superhéroes del Universo Marvel creado por Stan Lee y Jack Kirby. Hicieron su primera aparición en septiembre de 1963, en las páginas del cómic (The Uncanny) X-Men # 1.2\u200B\n" +
                        "Sus aventuras han sido publicadas en diversos cómics de Marvel Comics, llegando a disfrutar de varias colecciones mensuales dedicadas a ellos. Además, se han realizado varias adaptaciones en películas, series de animación y otras obras derivadas.");
                portada = BitmapFactory.decodeResource(getResources(), R.drawable.xmen);
                imgPortadaPeliculaDet.setImageBitmap(portada);
                txt_title.setText("X-men: Days of Future Past");
                txtTitEstrenoDet.setText("Details");
                txtEstrenoDet.setText("Released");
                txtFechaEstreno.setText("May 1, 2015");

                break;
        }

        //toolBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar_detalle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Movies Details");
        //getSupportActionBar().setIcon(R.drawable.ic_polymer_white_24dp);
    }//fin onCreate

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detalle_pelicula, menu);
        return true;
    }
}
