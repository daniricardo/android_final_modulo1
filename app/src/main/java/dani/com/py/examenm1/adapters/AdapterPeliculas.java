package dani.com.py.examenm1.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import dani.com.py.examenm1.R;
import dani.com.py.examenm1.entities.Peliculas;

/**
 * Created by Dell on 28/11/2017.
 */

public class AdapterPeliculas extends BaseAdapter {
    private Context _context; //contexto de la activity
    private List<Peliculas> _listPeliculas; //lista de peliculas
    Peliculas peliculas;

    ViewHolder holder = null;

    public AdapterPeliculas (Context context, List<Peliculas> listtChildData){
        this._listPeliculas = listtChildData;
        this._context = context;
    }



    @Override
    public int getCount() {
        return _listPeliculas.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try{
            peliculas = _listPeliculas.get(position); //recibo los datos, listado de peliculas

            if(convertView == null){
                LayoutInflater infaInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infaInflater.inflate(R.layout.activity_pelicula_item, null);

                holder = new ViewHolder();
                holder.img_portada = (ImageView) convertView.findViewById(R.id.img_portada);
                holder.linearL = (LinearLayout) convertView.findViewById(R.id.linearLa);
                holder.text_titulo = (TextView) convertView.findViewById(R.id.text_titulo);
                holder.text_rating = (TextView) convertView.findViewById(R.id.text_rating);
                holder.text_genero = (TextView) convertView.findViewById(R.id.text_genero);
                holder.text_anho = (TextView) convertView.findViewById(R.id.text_anho);
                convertView.setTag(holder);
                Log.d("ADAPTER", "CREA VISTA");
            }else{
                holder = (ViewHolder) convertView.getTag();//OBGIENE EL ITEM GUARDADO
                Log.d("ADAPTER", "VUELVE DEL RECYCLER");
            }
            //se asignan los datos a la vista
            holder.img_portada.setBackgroundResource(peliculas.getImagen());
            holder.text_titulo.setText(peliculas.getTitulo());
            holder.text_rating.setText(peliculas.getRating());
            holder.text_genero.setText(peliculas.getGenero());
            holder.text_anho.setText(peliculas.getAnho());

            if(peliculas.getState() == 1){
                holder.linearL.setBackgroundColor(_context.getResources().getColor(R.color.notif_color));
            }


        }catch (Throwable e){
            e.printStackTrace();
        }

        return convertView; // se retorna el item creado con sus datos
    }






    static class ViewHolder{
        ImageView img_portada;
        LinearLayout linearL;
        TextView text_titulo;
        TextView text_rating;
        TextView text_genero;
        TextView text_anho;


    }
}
