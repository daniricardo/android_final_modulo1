package dani.com.py.examenm1;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import dani.com.py.examenm1.adapters.AdapterPeliculas;
import dani.com.py.examenm1.entities.Peliculas;

/**
 * Created by Dell on 29/11/2017.
 */

public class MostrarPeliActivity extends AppCompatActivity {
    List<Peliculas> listItemsPeliculas = new ArrayList<>(); // lista de películas

    ListView listPeliculas;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_peliculas);

        cargarCarteleraPeliculas();
        listPeliculas = (ListView) findViewById(R.id.listaPeliculas);
        listPeliculas.setAdapter(new AdapterPeliculas(getApplicationContext(), listItemsPeliculas));

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_polymer_white_24dp);


        listPeliculas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int positionItem, long l) {
                Bundle bundle = new Bundle();
                switch (positionItem){
                    case 0:
                        bundle.putString("movie_name", "Dawn of the Planet of the Apes");
                        break;

                    case 1:
                        bundle.putString("movie_name", "District 9");
                        break;

                    case 2:
                        bundle.putString("movie_name", "Transformers: Age of Extinction");
                        break;

                    case 3:
                        bundle.putString("movie_name", "X-men: Days of Future Past");
                        break;
                }
                Intent intent = new Intent(MostrarPeliActivity.this, MostrarDetallePeliActivity.class);
                intent.putExtras(bundle);
                startActivity (intent);

            }
        });



    }//fin onCreate
/*
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }*/

    public void cargarCarteleraPeliculas(){
            Peliculas peliculas = new Peliculas();
            peliculas.setImagen(R.drawable.simios);
            peliculas.setTitulo("Dawn of the Planet of the Apes");
            peliculas.setRating("Rating: 8.3");
            peliculas.setGenero("Action, Drama, Sci-fi");
            peliculas.setAnho("2014");
            listItemsPeliculas.add(peliculas);

            peliculas = new Peliculas();
            peliculas.setImagen(R.drawable.distrito9);
            peliculas.setTitulo("District 9");
            peliculas.setRating("Rating: 8.0");
            peliculas.setGenero("Action, Sci-fi, Thriller");
            peliculas.setAnho("2009");
            listItemsPeliculas.add(peliculas);

            peliculas = new Peliculas();
            peliculas.setImagen(R.drawable.optimus);
            peliculas.setTitulo("Transformers: Age of Extinction");
            peliculas.setRating("Rating: 6.3");
            peliculas.setGenero("Action, Adventure, Sci-Fi");
            peliculas.setAnho("2014");
            listItemsPeliculas.add(peliculas);

            peliculas = new Peliculas();
            peliculas.setImagen(R.drawable.xmen);
            peliculas.setTitulo("X-men: Days of Future Past");
            peliculas.setRating("Rating: 8.4");
            peliculas.setGenero("Action, Sci-fi, Thriller");
            peliculas.setAnho("2014");
            listItemsPeliculas.add(peliculas);

        }


}
